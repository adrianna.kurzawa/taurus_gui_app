from setuptools import find_packages, setup

setup(
    name="tg",
    packages=find_packages(),
    entry_points={
        "gui_scripts": [
            "tg=tg.run_app:run_tg",
        ]
    },
)
